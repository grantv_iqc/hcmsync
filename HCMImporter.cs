﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace HCM_Sync
{

    class HCMImporter
    {
        private enum Titles {
            Mr = 1,
            Mrs = 2,
            Miss = 3,
            Ms = 4,
            Dr = 5,
            Prof = 6
        }

        private enum EmploymentStatus
        {
            EmployeeNotWithCompany = 0,
            EmployeeWithCompanyButInactive = 1,
            EmployeeWithCompanyButRetiree = 2,
            EmployeeWithCompanyAndActive = 3

        }

        public HCMObject Extract = new HCMObject();
        private XDocument _xmlDocLinq = new XDocument();
        private static XElement _E100000 = new XElement("E1P0000");
        private static XElement _E1P0001 = new XElement("E1P0001");
        private static XElement _E1P0002 = new XElement("E1P0002");
        private static XElement _E1P0105 = new XElement("E1P0105");

        public HCMImporter(string filename)
        {
            ValidateXml(filename, "HRMD_A.HRMD_A09.xsd");
            Initialize(filename);
        }

        private void Initialize(string filename)
        {
            LoadXml(filename);
            ExtractData();
            
        }
        
        private void ExtractData()
        {
            //XmlNode node = _xmlDoc
            //        .SelectSingleNode(_commenceDateLookup);

            if (_xmlDocLinq != null)
            {
                _E100000 = _xmlDocLinq.Descendants("E1P0000").FirstOrDefault();

                _E1P0001 = _xmlDocLinq.Descendants("E1P0001")
                                   .OrderByDescending(e => DateTime.ParseExact(e.Descendants("BEGDA").FirstOrDefault().Value, "yyyyMMdd", CultureInfo.InvariantCulture))
                                   .OrderByDescending(e => DateTime.ParseExact(e.Descendants("ENDDA").FirstOrDefault().Value, "yyyyMMdd", CultureInfo.InvariantCulture))
                                   .FirstOrDefault();

                _E1P0002 = _xmlDocLinq.Descendants("E1P0002").FirstOrDefault();

                _E1P0105 = _xmlDocLinq.Descendants("E1P0105").FirstOrDefault();

                // IDNumber
                if (_E1P0002.Descendants("PERID").Any()) { Extract.IDNumber = _E1P0002.Descendants("PERID").FirstOrDefault().Value; }

                // EmployeeNr
                if(_E100000.Descendants("PERNR").Any()) { Extract.EmployeeNr = _E100000.Descendants("PERNR").FirstOrDefault().Value; } 

                // Title
                var titleid = _E1P0002.Descendants("ANRED").FirstOrDefault().Value;
                if(_E1P0002.Descendants("ANRED").Any())
                {
                    var titleidparsed = int.Parse(_E1P0002.Descendants("ANRED").FirstOrDefault().Value); // parse string to integer
                    Extract.Title = Enum.GetName(typeof(Titles), titleidparsed); // lookup Title from Enum
                }

                // FirstName
                if(_E1P0002.Descendants("VORNA").Any())
                {
                    Extract.FirstName = _E1P0002.Descendants("VORNA").FirstOrDefault().Value;
                    // Alias - if Alias = null then default to FirstName - so check during FirstName lookup
                    if(_E1P0002.Descendants("RUFNM").Any())
                    {
                        Extract.Alias = _E1P0002.Descendants("RUFNM").FirstOrDefault().Value;
                    } else
                    {
                        Extract.Alias = Extract.FirstName; // if null then make the value FirstName
                    }
                }
                
                // Initials
                if(_E1P0002.Descendants("INITS").Any()) { Extract.Initials = _E1P0002.Descendants("INITS").FirstOrDefault().Value; }

                // LastName
                if(_E1P0002.Descendants("NACHN").Any()) { Extract.LastName = _E1P0002.Descendants("NACHN").FirstOrDefault().Value; }

                // Email
                if(_E1P0105.Descendants("USRID_LONG").Any()) { Extract.Email = _E1P0105.Descendants("USRID_LONG").FirstOrDefault().Value; }

                // Position
                if (_E1P0001.Descendants("PLANS").Any()) { Extract.Position = _E1P0001.Descendants("PLANS").FirstOrDefault().Value; }
                
                //JobTitle
                if (_E1P0001.Descendants("STEXT_P").Any()) { Extract.JobTitle = _E1P0001.Descendants("STEXT_P").FirstOrDefault().Value; }

                // Company
                if (_E1P0001.Descendants("NAME1").Any()) { Extract.Company = _E1P0001.Descendants("NAME1").FirstOrDefault().Value; }

                // Department
                if (_E1P0001.Descendants("STEXT_O").Any()) { Extract.Department = _E1P0001.Descendants("STEXT_O").FirstOrDefault().Value; }

                // JobCode
                if (_E1P0001.Descendants("STELL").Any()) { Extract.JobCode = _E1P0001.Descendants("STELL").FirstOrDefault().Value; }

                // CostCentreCode
                if (_E1P0001.Descendants("KOSTL").Any()) { Extract.CostCentreCode = _E1P0001.Descendants("KOSTL").FirstOrDefault().Value; }
                
                // EmployeeStatus
                if (_E1P0002.Descendants("ANRED").Any())
                {
                    var employeestatusid = int.Parse(_E1P0002.Descendants("ANRED").FirstOrDefault().Value); // parse string to integer
                    Extract.EmploymentStatus = Enum.GetName(typeof(EmploymentStatus), employeestatusid); // lookup Title from Enum
                }
                
                // CommenceDate
                if (_E100000.Descendants("BEGDA").Any()) { Extract.CommenceDate = _E100000.Descendants("BEGDA").FirstOrDefault().Value; }

                // EndDate
                if (_E100000.Descendants("ENDDA").Any()) { Extract.EndDate = _E100000.Descendants("ENDDA").FirstOrDefault().Value; }


            }
        }


        private void LoadXml(string filename)
        {
           
            var doc = XDocument.Load(filename);
            _xmlDocLinq = doc;
        }

        private void ValidateXml(string filename, string xsdfilename)
        {
            XmlReaderSettings hcmSettings = new XmlReaderSettings();
            hcmSettings.Schemas.Add("http://www.w3.org/2001/XMLSchema", xsdfilename);
            hcmSettings.ValidationType = ValidationType.Schema;
            hcmSettings.ValidationEventHandler += new ValidationEventHandler(hcmSettingsValidationEventHandler);
            hcmSettings.Schemas.Compile();

            XmlReader hcmFeed = XmlReader.Create(filename, hcmSettings);

            while (hcmFeed.Read()) { }

            hcmFeed.Close();
        }

        static void hcmSettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.Write("WARNING: ");
                Console.WriteLine(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {
                Console.Write("ERROR: ");
                Console.WriteLine(e.Message);
            }
        }

        protected static void serializer_UnknownNode
            (object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        protected static void serializer_UnknownAttribute
            (object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

}
