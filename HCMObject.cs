﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCM_Sync
{
    class HCMObject
    {
        public string IDNumber { get; set; }
        public string EmployeeNr { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Alias { get; set; }
        public string Initials { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Position { get; set; }
        public string JobTitle { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string JobCode { get; set; }
        public string CostCentreCode { get; set; }
        public string EmploymentStatus { get; set; }
        public string CommenceDate { get; set; }
        public string EndDate { get; set; }


    }
}
