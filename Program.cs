using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace HCM_Sync
{
    class Program
    {

        static void Main(string[] args)
        {
            {
                HCMImporter importer = new HCMImporter("HRMD_A09_test.xml");

                Console.WriteLine("------------------------------ First Test ----------------------");
                Console.WriteLine("ID Number: " + importer.Extract.IDNumber);
                Console.WriteLine("Employee Number: " + importer.Extract.EmployeeNr);
                Console.WriteLine("Title: " + importer.Extract.Title);
                Console.WriteLine("First Name: " + importer.Extract.FirstName); 
                Console.WriteLine("Alias: " + importer.Extract.Alias);
                Console.WriteLine("Initials: " + importer.Extract.Initials);
                Console.WriteLine("Last Name: " + importer.Extract.LastName);
                Console.WriteLine("Email: " + importer.Extract.Email);
                Console.WriteLine("Position Code: " + importer.Extract.Position);
                Console.WriteLine("Job Title: " + importer.Extract.JobTitle);
                Console.WriteLine("Company: " + importer.Extract.Company);
                Console.WriteLine("Department: " + importer.Extract.Department);
                Console.WriteLine("Job Code: " + importer.Extract.JobCode);
                Console.WriteLine("Cost Centre Code: " + importer.Extract.CostCentreCode);
                Console.WriteLine("Employment Status: " + importer.Extract.EmploymentStatus);
                Console.WriteLine("Commence Date: " + importer.Extract.CommenceDate);
                Console.WriteLine("End Date: " + importer.Extract.EndDate);

                Console.ReadKey();

                HCMImporter importer2 = new HCMImporter("HRMD_A09_test2.xml");

                Console.WriteLine("------------------------------ Second Test ----------------------");
                Console.WriteLine("ID Number: " + importer2.Extract.IDNumber);
                Console.WriteLine("Employee Number: " + importer2.Extract.EmployeeNr);
                Console.WriteLine("Title: " + importer2.Extract.Title);
                Console.WriteLine("First Name: " + importer2.Extract.FirstName);
                Console.WriteLine("Alias: " + importer2.Extract.Alias);
                Console.WriteLine("Initials: " + importer2.Extract.Initials);
                Console.WriteLine("Last Name: " + importer2.Extract.LastName);
                Console.WriteLine("Email: " + importer2.Extract.Email);
                Console.WriteLine("Position Code: " + importer2.Extract.Position);
                Console.WriteLine("Job Title: " + importer2.Extract.JobTitle);
                Console.WriteLine("Company: " + importer2.Extract.Company);
                Console.WriteLine("Department: " + importer2.Extract.Department);
                Console.WriteLine("Job Code: " + importer2.Extract.JobCode);
                Console.WriteLine("Cost Centre Code: " + importer2.Extract.CostCentreCode);
                Console.WriteLine("Employment Status: " + importer2.Extract.EmploymentStatus);
                Console.WriteLine("Commence Date: " + importer2.Extract.CommenceDate);
                Console.WriteLine("End Date: " + importer2.Extract.EndDate);

                Console.ReadKey();

                HCMImporter importer3 = new HCMImporter("HRMD_A09_test3.xml");

                Console.WriteLine("------------------------------ Third Test ----------------------");
                Console.WriteLine("ID Number: " + importer3.Extract.IDNumber);
                Console.WriteLine("Employee Number: " + importer3.Extract.EmployeeNr);
                Console.WriteLine("Title: " + importer3.Extract.Title);
                Console.WriteLine("First Name: " + importer3.Extract.FirstName);
                Console.WriteLine("Alias: " + importer3.Extract.Alias);
                Console.WriteLine("Initials: " + importer3.Extract.Initials);
                Console.WriteLine("Last Name: " + importer3.Extract.LastName);
                Console.WriteLine("Email: " + importer3.Extract.Email);
                Console.WriteLine("Position Code: " + importer3.Extract.Position);
                Console.WriteLine("Job Title: " + importer3.Extract.JobTitle);
                Console.WriteLine("Company: " + importer3.Extract.Company);
                Console.WriteLine("Department: " + importer3.Extract.Department);
                Console.WriteLine("Job Code: " + importer3.Extract.JobCode);
                Console.WriteLine("Cost Centre Code: " + importer3.Extract.CostCentreCode);
                Console.WriteLine("Employment Status: " + importer3.Extract.EmploymentStatus);
                Console.WriteLine("Commence Date: " + importer3.Extract.CommenceDate);
                Console.WriteLine("End Date: " + importer3.Extract.EndDate);

                Console.ReadKey();

            }
            
            Console.ReadKey();
        }

        


        


    }
}
